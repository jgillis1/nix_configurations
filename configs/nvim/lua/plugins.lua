-- Required if packer is in `opt` path
vim.cmd [[packadd packer.nvim]]

local packer = require('packer')

return packer.startup(function()
  local use = packer.use

  -- Packer
  use { 'wbthomason/packer.nvim', opt = true }

  use { 'tjdevries/astronauta.nvim' }

  -- Fuzzy Finder
  use { 'nvim-telescope/telescope.nvim',
    requires = {
      { 'nvim-lua/plenary.nvim' },
      { 'nvim-lua/popup.nvim' },
      { 'nvim-telescope/telescope-github.nvim',
        config = function()
          require'telescope'.load_extension("gh")
        end
      },
    },
  }

 -- Tree-sitter
	use {
		'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate',
		config = function() require'treesitter' end,
	}


  -- colorschemes
  use { 'junegunn/seoul256.vim' }
  use { 'morhetz/gruvbox' }
  use { 'NLKNguyen/papercolor-theme' }
  use { 'ayu-theme/ayu-vim' }

  -- Utilities

    -- LSPs and Completion
  use {
    'neovim/nvim-lspconfig',
    config = function() require('lsp') end,
    requires = {
      { 'nvim-lua/completion-nvim' },
      { 'nvim-lua/lsp_extensions.nvim' },
      { 'nvim-lua/lsp-status.nvim' },
      { 'steelsojka/completion-buffers' },
    },
  }

  use { 'tpope/vim-vinegar' }
  use { 'glepnir/lspsaga.nvim' }
  use { 'fatih/vim-go', run = ':GoUpdateBinaries' }
  use { 'Shougo/echodoc.vim' }
  use { 'SirVer/ultisnips' }
  use { 'elixir-editors/vim-elixir' }
  use { 'godlygeek/tabular' }
  use { 'plasticboy/vim-markdown' }
  use { 'LnL7/vim-nix' }
  use { 'sheerun/vim-polyglot' }
  use { 'tpope/vim-dispatch' }
  use { 'tpope/vim-surround' }
  use { 'tpope/vim-fugitive' }
  use { 'tpope/vim-commentary' }
  use { 'nathanaelkane/vim-indent-guides' }
  use { 'majutsushi/tagbar' }
  use { 'vim-scripts/YankRing.vim' }
  use { 'ryanoasis/vim-devicons' }
  use { 'vim-scripts/Align' }
  use { 'vim-scripts/AnsiEsc.vim' }
  use { 'junegunn/gv.vim' }
  use { 'nikvdp/neomux' }
  use { 'christoomey/vim-system-copy' }
  use {
    'lewis6991/gitsigns.nvim', requires = { 'nvim-lua/plenary.nvim' },
    config = function() require('gitsigns').setup() end
  }

  use {
  'glepnir/galaxyline.nvim',
    branch = 'main',
    -- your statusline
    config = function() require'statusline' end,
    -- some optional icons
    requires = {'kyazdani42/nvim-web-devicons', opt = true}
  }
  
  -- You can specify multiple plugins in a single call
  use { 'tjdevries/colorbuddy.vim' }
end)
