local treesitter = require'nvim-treesitter.configs'

treesitter.setup {
	defaults = {
		prompt_prefix = "top",
		file_ignore_patterns = {".git/*", "node_modules/*"},
		color_devicons = true,
		shorten_path = true
	},
	ensure_installed = {
		"go",
		"lua",
		"json",
		"rust",
		"toml",
		"yaml"
	},
	highlight = {
		enable = true
	},
	incremental_selection = {
		enable = false
	},
	indent = {
		enable = true
	},
}
