lua require('plugins')

autocmd BufWritePost plugins.lua PackerCompile
" Use completion-nvim in every buffer
autocmd BufEnter * lua require('completion').on_attach()
autocmd FileType lua setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
autocmd FileType yaml setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab

" Line numbering
set number
set relativenumber

" Tabbing
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

" Start scrolling when the cursor is 3 lines from the bottom of the buffer
set scrolloff=3

" Allow the cursor to go anywhere in the buffer
set virtualedit=all

" command area height
set cmdheight=1

" Allow backspacing
set backspace=indent,eol,start

" Status line
set laststatus=2

" Disable clearing scrollback buffer when exiting vim
set t_ti= t_te=
set t_ut=''

" Searching
set incsearch
set hlsearch
set ignorecase

" Recognize file types for syntax highlighting
filetype plugin on

let g:polyglot_disabled = ['elixir']

" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Set completeopt to have a better completion experience
set completeopt=menuone,noinsert,noselect

" Avoid showing message extra message when using completion
set shortmess+=c

" possible value: 'UltiSnips', 'Neosnippet', 'vim-vsnip', 'snippets.nvim'
let g:completion_enable_snippet = 'UltiSnips'
let g:go_def_mapping_enabled = 0

" Definition/reference finder
nnoremap <silent> gh <cmd>lua require'lspsaga.provider'.lsp_finder()<CR>
" Code Action
nnoremap <silent><leader>ca <cmd>lua require('lspsaga.codeaction').code_action()<CR>`
" Hover Docs
nnoremap <silent> K <cmd>lua vim.lsp.buf.hover()<CR>
" preview definition
nnoremap <silent> gd <cmd>lua require'lspsaga.provider'.preview_definition()<CR>
" jump diagnostic
nnoremap <silent> [e <cmd>lua require'lspsaga.diagnostic'.lsp_jump_diagnostic_prev()<CR>
nnoremap <silent> ]e <cmd>lua require'lspsaga.diagnostic'.lsp_jump_diagnostic_next()<CR>
" float terminal also you can pass the cli command in open_float_terminal function
nnoremap <silent> <A-d> <cmd>lua require('lspsaga.floaterm').open_float_terminal('lazygit')<CR>
tnoremap <silent> <A-d> c:lua require('lspsaga.floaterm').close_float_terminal()<CR>
noremap <F2> :NvimTreeToggle<CR>
nnoremap <leader>r :NvimTreeRefresh<CR>
nnoremap <leader>n :NvimTreeFindFile<CR>
highlight NvimTreeFolderIcon guibg=blue
set conceallevel=2

" Colors
set termguicolors
set background=dark
" colorscheme seoul256-light
" let ayucolor="light"
" colorscheme ayu
colorscheme gruvbox
hi Comment cterm=italic

set cursorline
set foldmethod=syntax

" Store swap files in /tmp
set directory=/tmp

" Visualize tabs, trailing whitespaces and funny characters
" http://www.reddit.com/r/programming/comments/9wlb7/proggitors_do_you_like_the_idea_of_indented/c0esam1
" https://wincent.com/blog/making-vim-highlight-suspicious-characters
set list
set listchars=nbsp:¬,tab:»·,trail:·

" Turn on syntax highlighting
syntax on

" toggle search highlighting
noremap <F4> :set hlsearch! <cr>

" toggle Tagbar
noremap <F8> :TagbarToggle <CR>

" toggle Gundo
nnoremap <F5> :GundoToggle<CR>

nnoremap <leader>y :YRShow<CR>

" Build tags for current directory using exuberant c-tags
nmap ,ta :w \|!ctags -R . <cr>

" Get back to alt buffer
nmap ,, <C-^>

" contents of last global command in new window
nmap <F3> :redir @a<CR>:g//<CR>:redir END<CR>:new<CR>:put! a<CR><CR>

" json liniting requires jsonlint https://github.com/zaach/jsonlint
nmap ,j :w\|!jsonlint -q % <CR>

" my common quit and write problems
nmap :Q :q
nmap :W :w

" Get rid of trailing whitespace
nmap ,de :%s= *$==<cr>

set rtp+=~/.fzf
" nnoremap <leader>f :GFiles<CR>
" nnoremap <leader>d :Files<CR>
" nnoremap <Leader>e :Buffers<CR>
" nnoremap <Leader>s :SK<CR>
" nnoremap <Leader>g :Commits<CR>
"
" Find files using Telescope command-line sugar.
nnoremap <leader>f :Telescope find_files<cr>
nnoremap <leader>g :Telescope live_grep<cr>
nnoremap <leader>e :Telescope buffers<cr>
nnoremap <leader>h :Telescope help_tags<cr>

augroup update_bat_theme
    autocmd!
    autocmd colorscheme * call ToggleBatEnvVar()
augroup end
function ToggleBatEnvVar()
    if (&background == "light")
        let $BAT_THEME='Monokai Extended Light'
    else
        let $BAT_THEME=''
    endif
endfunction

" Indent guides settings
hi IndentGuidesOdd  ctermbg=white
hi IndentGuidesEven ctermbg=darkgrey
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
let g:vim_markdown_folding_disabled = 1

let g:go_highlight_extra_types = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_parameters = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_types = 1
let g:go_highlight_fields = 1

set noshowmode
set foldlevelstart=20

au BufRead,BufNewFile *.md setlocal textwidth=80
autocmd BufWritePost ~/journals/**/*.md :helptags ~/.vim/doc
au BufRead,BufNewFile *.wiki setlocal textwidth=80
au BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile
autocmd FileType gitcommit set bufhidden=delete

set hidden

autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4
