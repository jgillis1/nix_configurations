# NixOS Configuration

This is the nixos configuration and installation method I use to boostrap new
systems.  Inspiration for this setup was taken from [this blog post](https://grahamc.com/blog/erase-your-darlings).


## Installation
This installation method assumes that you are already familiar with the NixOS
installation procedure.  If you are not, you can read about that [here](https://nixos.org/manual/nixos/stable/).
Further information on configuration options can be obtained by running `man
configuration.nix` from a terminal in the live boot environment.

When booted into the NixOS live environment, you can run the `install_nixos.sh`
script to partition the non-partitioned disk passed in as the first argument:
`./install_nixos.sh /dev/disk/by-id/<disk-without-parition-here>`

This method assumes a single disk installation and creates parititons for EFI booting and
ZFS root filesystems.  Please read The install script and verify that it will
do what you expect.  Use at your own risk!  I am not responsible for any damage
to your disks.

The example `configuration.nix` file can be used to create a working system.
You should update the settings to match your needs and generate a hashed
password for your user.  This is needed to create the user after every reboot.
