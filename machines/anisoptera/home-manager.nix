
{ config, lib, pkgs, ... }:
let
  home-manager = builtins.fetchTarball {
    url = "https://github.com/rycee/home-manager/archive/master.tar.gz";
};

in {
  imports = [
    "${home-manager}/nixos"
  ];

  home-manager.users.jgillis = { pkgs, ... }: {
    programs.home-manager.enable = true;

    programs.git = {
      enable = true;
      userName = "Jeffrey Gillis";
      userEmail = "jgillis@optoro.com";
      package = pkgs.gitAndTools.gitFull;
      signing = {
        key = "C579BA41787813C8CD67D0FE5AC69BFFFC41C7F0";
        signByDefault = true;
      };
      aliases = {
        branches = "branch -a";
        tags = "tag";
        stashes = "stash list";
        unstage = "reset -q HEAD --";
        discard = "checkout --";
        uncommit = "reset --mixed HEAD~";
        amend = "commit --amend";
        nevermind = "!git reset --hard HEAD && git clean -d -f";
        graph = "log --graph --oneline -10";
        lg = "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'";
        gss = "git status -s";
      };
      delta = {
        enable = true;
      };
      extraConfig = {
        pull = { ff = "only"; };
      };
    };

    programs.gpg.enable = true;

    # programs.neovim = {
    #   enable = true;
    #   vimAlias = true;
    #   vimdiffAlias = true;
    #   configure = {
    #   	customRC = builtins.readFile ../../configs/vimrc;
    #   };
    # };

    programs.bat = {
      enable = true;
      config = {
        theme = "TwoDark";
      };
    };

    programs.jq = {
      enable = true;
    };

    programs.skim = {
      enable = true;
      fileWidgetCommand = "fd --type f";
      fileWidgetOptions = [ "--preview 'head {}'" ];
      changeDirWidgetCommand = "fd --type d";
      changeDirWidgetOptions = [ "--preview 'tree -C {} | head -200'" ];
      historyWidgetOptions = [ "--tac" "--exact" ];
      enableZshIntegration = true;
    };

    programs.starship = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        aws.disabled = true;
        gcloud.disabled = true;
        package.disabled = true;
        character = {
          success_symbol = "->";
          error_symbol = "->";
        };
      };
    };

    programs.zoxide = {
      enable = true;
      enableZshIntegration = true;
    };

    programs.zsh = {
      enable = true;
      shellAliases = {
        l = "exa --group-directories-first --classify --git";
        ll = "l -l";
        lla = "l -la";
        vim = "nvim";
        gss = "git status -s";
        kctx = "kubectx";
        kns = "kubens";
        k = "kubectl";
        h = "helm";
        skh = "f(){ rg \"\$1\" \${HOME}/history/**/* | sk -m --ansi | cut -d':' -f2-}; f";
	ve = "f(){ rg . \"\$1\" | sk -m --ansi | cut -d':' -f1 | xargs -exec vim }; f";
        fs = "fshow_preview";
        v = "nvr";
        vn = "nvim +Neomux";
        gp = "gopass";
        tf = "terraform";
        ff = "firefox --new-tab $(rg . $HOME/firefox_urls.txt | rg -v \"https://accounts|calendar|mail|drive|www\.google\.com\" | sk)";
        cbh = "create_browser_history";
      };

      envExtra = ''
        PROMPT_COMMAND="write_history";
        GPGKEY="47FB9111";
        SKIM_DEFAULT_OPTIONS="--preview-window right:70% --bind '?:toggle-preview,ctrl-o:execute-silent(xdg-open {})'"
        SKIM_CTRL_T_OPTS="--preview 'bat --color always {}'"
        export PATH="$PATH:$HOME/.local/bin"
        export PATH="$PATH:$HOME/local/bin"
        export GOPATH="$HOME/go"
        export PATH="$GOPATH/bin:$PATH"
        export OPTORO_TEAM_NAME=devops
        export AWS_DEFAULT_REGION=us-east-1
        export AWS_PROFILE=default
        export EDITOR=vim
      '';

      initExtra = ''
        source <(kubectl completion zsh)
        precmd() { eval "$PROMPT_COMMAND" }
        bindkey '^E' skim-file-widget
        complete -F __start_kubectl k
        export NIX_PATH="$HOME/.nix-defexpr/channels:$NIX_PATH"
      '';
    };

    programs.obs-studio = {
      enable = true;
      plugins = [
        pkgs.obs-v4l2sink
      ];
    };

    # Home Manager needs a bit of information about you and the
    # paths it should manage.
    home.username = "jgillis";
    home.homeDirectory = "/home/jgillis";

    systemd.user.tmpfiles.rules = [
      "d /home/jgillis/zsh 0755 jgillis users - -"
      "d /home/jgillis/local 0755 jgillis users - -"
      "L+ /home/jgillis/.aws - - - - /persist/jgillis/.aws"
      "L+ /home/jgillis/.config/nvim - - - - /persist/jgillis/nix-configuration/configs/nvim"
      "L+ /home/jgillis/Development - - - - /persist/jgillis/Development"
      "L+ /home/jgillis/.gitlab-api-token - - - - /persist/jgillis/.gitlab-api-token"
      "L+ /home/jgillis/history - - - - /persist/jgillis/history"
      "L+ /home/jgillis/.kube - - - - /persist/jgillis/.kube"
      "L+ /home/jgillis/local/bin - - - - /persist/jgillis/local/bin"
      "L+ /home/jgillis/.local/share/konsole - - - - /persist/jgillis/.local/share/konsole"
      "L+ /home/jgillis/openvpn - - - - /persist/jgillis/openvpn"
      "L+ /home/jgillis/.optoro-store - - - - /persist/jgillis/.optoro-store"
      "L+ /home/jgillis/Pictures - - - - /persist/jgillis/Pictures"
      "L+ /home/jgillis/.rancher-cli-token - - - - /persist/jgillis/.rancher-cli-token"
      "L+ /home/jgillis/.ssh - - - - /persist/jgillis/.ssh"
      "L+ /home/jgillis/.tmux.conf.local - - - - /persist/jgillis/.tmux.conf.local"
      "L+ /home/jgillis/.tmux.conf - - - - /persist/jgillis/.tmux.conf"
      "L+ /home/jgillis/zsh/functions - - - - /persist/jgillis/zsh/functions"
    ];

    nixpkgs.config.allowUnfree = true;

    home.packages = with pkgs; [
      awscli
      (callPackage ../../pkgs/bcrypt {}).bcrypt
      chezmoi
      choose
      (callPackage ../../pkgs/create_browser_history {}).create_browser_history
      cryptsetup
      ctags
      du-dust
      elixir
      exa
      fd
      gcc
      go
      google-cloud-sdk
      gopass
      hugo
      k9s
      kubectl
      kubectx
      kubernetes-helm
      (callPackage ../../pkgs/lab {}).lab
      lua
      neovim
      neovim-remote
      nodejs
      obs-studio
      obs-v4l2sink
      openvpn
      pstree
      ripgrep
      slack
      sops
      sqlite
      terraform
      (callPackage ../../pkgs/terraform-docs {}).terraform-docs
      tmate
      tmux
      tree
      vault
      (callPackage ../../pkgs/write_history {}).history_log
      yarn
      zoom-us
    ];

    nixpkgs.overlays = [
      (self: super: {
        tree-sitter-updated = super.tree-sitter.overrideAttrs(oldAttrs: {
          version = "0.18.0";
          sha256 = "sha256-uQs80r9cPX8Q46irJYv2FfvuppwonSS5HVClFujaP+U=";
          cargoSha256 = "sha256-fonlxLNh9KyEwCj7G5vxa7cM/DlcHNFbQpp0SwVQ3j4=";

          postInstall = ''
            PREFIX=$out make install
          '';
        });

        neovim-unwrapped = super.neovim-unwrapped.overrideAttrs (oldAttrs: rec {
          name = "neovim-nightly";
          version = "0.5-nightly";
          src = self.fetchurl {
            url = "https://github.com/neovim/neovim/archive/nightly.zip";
            sha256 = "sha256:0kd52f6j6yvv5iwzs7i9qzp2r5zw5hddvbn5bjjshazw1ghvhv0q";
          };
          nativeBuildInputs = with self.pkgs; [ unzip cmake pkgconfig gettext tree-sitter-updated ];
        });
      })
    ];

      # This value determines the Home Manager release that your
      # configuration is compatible with. This helps avoid breakage
      # when a new Home Manager release introduces backwards
      # incompatible changes.
      #
      # You can update Home Manager without changing this value. See
      # the Home Manager release notes for a list of state version
      # changes in each release.
      home.stateVersion = "21.03";
    };
  }
