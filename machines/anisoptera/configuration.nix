# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:
let
  machine = "anisoptera";

in {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./home-manager.nix
      ../../modules/system.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.supportedFilesystems = [ "zfs" ];
  boot.initrd.postDeviceCommands = lib.mkAfter ''
    zfs rollback -r rpool/local/root@blank
  '';

  boot.extraModulePackages = [
    config.boot.kernelPackages.v4l2loopback
  ];

  boot.kernelModules = [
    "v4l2loopback"
  ];

  boot.extraModprobeConfig = ''
    options v4l2loopback video_nr=10 card_label="OBS Video Source" exclusive_caps=1
  '';

  networking.hostName = machine; # Define your hostname.
  networking.hostId = "ac22b0c8";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Set your time zone.
  time.timeZone = "America/New_York";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.wlp59s0.useDHCP = true;
  networking.networkmanager.enable = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Enable the Plasma 5 Desktop Environment.
  services.xserver.enable = true;
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Configure keymap in X11
  services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  # services.printing.enable = true;
  # Enable log rotation
  services.logrotate = {
    enable = true;
    extraConfig = ''
      nocompress
    '';
    paths = {
      logs = {
        enable = true;
        path = "/var/log/**/*.log";
        frequency = "weekly";
        keep = 9;
      };
    };
  };


  # Enable auto upgrades
  system.autoUpgrade.enable = true;
  # Make sure machine id doesn't change between boots (needed for journald)
  systemd.tmpfiles.rules = [
    "L+ /etc/machine-id - - - - /persist/etc/machine-id"
    "L+ /etc/NetworkManager/system-connections/ghomeap.nmconnection - - - - /persist/etc/NetworkManager/ghomeap.nmconnection"
  ];

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
  virtualisation.docker = {
    enable = true;
    storageDriver = "zfs";
  };

  users.users.jgillis = {
    isNormalUser = true;
    shell = pkgs.zsh;
    hashedPassword = "$6$E50.gqH1BN$AtZgOCAYwgDrobn8PmY6bvjh0dgGe8Y7TXfqL4d02Ag40t.FvZ8HmIFBIaM4c7Ed5qJBdgSTJ72QsKAiv5c4P/";
    extraGroups = [ "wheel" "docker" ]; # Enable ‘sudo’ for the user.
  };

}

