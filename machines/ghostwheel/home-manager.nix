
{ config, lib, pkgs, ... }:
let
  home-manager = builtins.fetchTarball {
    url = "https://github.com/rycee/home-manager/archive/master.tar.gz";
};

in {
  imports = [
    "${home-manager}/nixos"
  ];

  home-manager.users.enoch = { pkgs, ... }: {
     # Let Home Manager install and manage itself.
    programs.home-manager.enable = true;

    programs.git = {
      enable = true;
      userName = "Jeffrey Gillis";
      userEmail = "jeffrey.gillis1@gmail.com";
      package = pkgs.gitAndTools.gitFull;
      signing = {
        key = "C579BA41787813C8CD67D0FE5AC69BFFFC41C7F0";
      };
      aliases = {
        branches = "branch -a";
        tags = "tag";
        stashes = "stash list";
        unstage = "reset -q HEAD --";
        discard = "checkout --";
        uncommit = "reset --mixed HEAD~";
        amend = "commit --amend";
        nevermind = "!git reset --hard HEAD && git clean -d -f";
        graph = "log --graph --oneline -10";
        gss = "git status -s";
      };
      delta = {
        enable = true;
      };
    };

    programs.gpg.enable = true;

    programs.neovim = {
      enable = true;
      vimAlias = true;
      vimdiffAlias = true;
      configure = {
        customRC = builtins.readFile ./configs/vimrc;
      };
    };

    programs.bat = {
      enable = true;
      config = {
        theme = "TwoDark";
      };
    };

    programs.jq = {
      enable = true;
    };

    programs.skim = {
      enable = true;
      fileWidgetCommand = "fd --type f";
      fileWidgetOptions = [ "--preview 'head {}'" ];
      changeDirWidgetCommand = "fd --type d";
      changeDirWidgetOptions = [ "--preview 'tree -C {} | head -200'" ];
      historyWidgetOptions = [ "--tac" "--exact" ];
      enableZshIntegration = true;
    };

    programs.starship = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        aws.disabled = true;
        gcloud.disabled = true;
        package.disabled = true;
        character.symbol = "->";
      };
    };

    programs.zoxide = {
      enable = true;
      enableZshIntegration = true;
    };

    programs.zsh = {
      enable = true;
      shellAliases = {
        l = "exa --group-directories-first --classify --git";
        ll = "l -l";
        lla = "l -la";
        vim = "nvim";
        gss = "git status -s";
        kctx = "kubectx";
        kns = "kubens";
        k = "kubectl";
        h = "helm";
        skh = "f(){ rg \"\$1\" \${HOME}/history/**/* | sk --ansi}; f";
        fs = "fshow_preview";
        v = "nvr";
        gp = "gopass";
        tf = "terraform";
        ff = "f(){ firefox --new-tab $(rg \"\$1\" \${HOME}/firefox_history.txt | sk --ansi)}; f";
      };

      envExtra = ''
        PROMPT_COMMAND="write_history";
        GPGKEY="47FB9111";
        SKIM_DEFAULT_OPTIONS="--preview-window right:70% --bind '?:toggle-preview,ctrl-o:execute-silent(xdg-open {})'"
        SKIM_CTRL_T_OPTS="--preview 'bat --color always {}'"
        export PATH="$PATH:$HOME/.local/bin"
        export PATH="$PATH:$HOME/local/bin"
        export GOPATH="$HOME/go"
        export PATH="$GOPATH/bin:$PATH"
        export OPTORO_TEAM_NAME=devops
        export EDITOR=vim
      '';

      initExtra = ''
        precmd() { eval "$PROMPT_COMMAND" }
        bindkey '^E' skim-file-widget
        export NIX_PATH="$HOME/.nix-defexpr/channels:$NIX_PATH"
      '';
    };

    home.username = "enoch";
    home.homeDirectory = "/home/enoch";

    systemd.user.tmpfiles.rules = [
      "d /home/enoch/zsh 0755 enoch users - -"
      "d /home/enoch/local 0755 enoch users - -"
      "L+ /home/enoch/.local/share/konsole 0755 enoch users - /home/enoch/.config/nixpkgs/configs/konsole"
      "L+ /home/enoch/zsh/functions 0755 enoch users - /home/enoch/.config/nixpkgs/configs/zsh_functions"
      "L+ /home/enoch/local/bin 0755 enoch users - /home/enoch/.config/nixpkgs/binaries"
      "L+ /home/enoch/.vimrc 0755 enoch users - /home/enoch/.config/nixpkgs/configs/vimrc"
      "L+ /home/enoch/.tmux.conf 0755 enoch users - /home/enoch/.config/nixpkgs/configs/tmux.conf"
      "L+ /home/enoch/.tmux.conf.local 0755 enoch users - /home/enoch/.config/nixpkgs/configs/tmux.conf.local"
    ];

    home.packages = with pkgs; [
      awscli
      ctags
      du-dust
      elixir
      exa
      fd
      ffmpeg
      gcc
      go
      google-cloud-sdk
      gopass
      hugo
      k9s
      kubectl
      kubectx
      kubernetes-helm
      neovim-remote
      nodejs
      pstree
      ripgrep
      slack
      sops
      sqlite
      terraform
      tmate
      tmux
      tree
      vault
      vlc
      yarn
      zoom-us
    ];

    nixpkgs.overlays = [
      (self: super: {
        tree-sitter-updated = super.tree-sitter.overrideAttrs(oldAttrs: {
          version = "0.17.3";
          sha256 = "sha256-uQs80r9cPX8Q46irJYv2FfvuppwonSS5HVClFujaP+U=";
          cargoSha256 = "sha256-fonlxLNh9KyEwCj7G5vxa7cM/DlcHNFbQpp0SwVQ3j4=";

          postInstall = ''
            PREFIX=$out make install
          '';
        });

        neovim-unwrapped = super.neovim-unwrapped.overrideAttrs (oldAttrs: rec {
          name = "neovim-nightly";
          version = "0.5-nightly";
          src = self.fetchurl {
            url = "https://github.com/neovim/neovim/archive/nightly.zip";
            sha256 = "sha256:16zd2fq2n1497iaj7wk7ncl0hm4gwdfln9gffq8nfx85i7cdmxxm";
          };
          nativeBuildInputs = with self.pkgs; [ unzip cmake pkgconfig gettext tree-sitter-updated ];
        });
      })
    ];

    home.stateVersion = "21.03"; 
    };

    home-manager.users.laura = { pkgs, ... }: {
       # Let Home Manager install and manage itself.
      programs.home-manager.enable = true;

      home.username = "laura";
      home.homeDirectory = "/home/laura";

      systemd.user.tmpfiles.rules = [ ];

      home.packages = with pkgs; [
        vscode
      ];

      home.stateVersion = "21.03"; 
      };
  }
