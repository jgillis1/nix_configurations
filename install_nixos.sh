#!/usr/bin/env bash
# This setup is inspired by the following blog post:
# https://grahamc.com/blog/erase-your-darlings

# Set Disk
# Pass in as /dev/disk/by-id/<disk without partitions>
DISK=$1

# Partition disk
sgdisk -n3:1M:+512M -t3:EF00 $DISK # EFI Partition
sgdisk -n1:0:0 -t1:BF01 $DISK # ZFS Partition

# Create encrypted zpool
zpool create -o ashift=12 -o altroot="/mnt" -O acltype=posixacl -O xattr=sa -O compression=lz4 -O mountpoint=none -O encryption=aes-256-gcm -O keyformat=passphrase rpool $DISK-part1

# Create and mount datasets
zfs create -p -o mountpoint=legacy rpool/local/root

# Create empty snapshots for rollbacks at boot
zfs snapshot rpool/local/root@blank

# Mount root data set
mount -t zfs rpool/local/root /mnt

# Mount boot partition (EFI parition)
mkdir /mnt/boot
mount $DISK-part3 /mnt/boot

# Create nix data set and mount it
zfs create -p -o mountpoint=legacy rpool/local/nix
mkdir /mnt/nix
mount -t zfs rpool/local/nix /mnt/nix

# Create home data set and mount it
zfs create -p -o mountpoint=legacy rpool/safe/home
mkdir /mnt/home
mount -t zfs rpool/safe/home /mnt/home

# Create a place to store persistent data and mount it
# You can use systemd.tmpfiles.rules to manage these later (if needed)
zfs create -p -o mountpoint=legacy rpool/safe/persist
mkdir /mnt/persist
mount -t zfs rpool/safe/persist /mnt/persist

# Create a nixos data set to store your nixos configuration
zfs create -p -o mountpoint=legacy rpool/safe/nixos
mkdir -p /mnt/etc/nixos
mount -t zfs rpool/safe/nixos /mnt/etc/nixos/

# Generate initial configuration and hardware config
nixos-generate-config --root /mnt
