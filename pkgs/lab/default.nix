{ stdenv, lib, buildGoModule, fetchFromGitHub, git }:
{
  lab = buildGoModule rec {
    pname = "lab";
    version = "0.18.0";

    src = fetchFromGitHub {
      owner = "zaquestion";
      repo = "lab";
      rev = "v${version}";
      sha256 = "sha256:0zkwvmzgj7h8lc8jkg2a81392b28c8hkwqzj6dds6q4asbmymx5c";
    };

    vendorSha256 = "sha256:1lrmafvv5zfn9kc0p8g5vdz351n1zbaqwhwk861fxys0rdpqskyc"; 

    subPackages = [ "." ]; 

    # deleteVendor = true; 

    # runVend = true; 

    doCheck = true;
    checkPhase = ''
    '';

    meta = with lib; {
      description = "Lab wraps Git or Hub, making it simple to clone, fork, and interact with repositories on GitLab, including seamless workflows for creating merge requests, issues and snippets.";
      maintainers = with maintainers; [ zaquestion ];
      platforms = platforms.linux;
    };
  };
}
