#!/usr/bin/env bash

# Make temp directory
mydir=$(mktemp -d)
db=$(find "${HOME}/.mozilla/firefox/" -name "places.sqlite")
cp $db $mydir/places.sqlite
#query="select p.url from moz_historyvisits as h, moz_places as p where substr(h.visit_date, 0, 11) >= strftime('%s', date('now')) and p.id == h.place_id order by h.visit_date;"
query="select p.url from moz_historyvisits as h, moz_places as p where p.id == h.place_id order by h.visit_date;"
todays_urls=$(sqlite3 "${mydir}/places.sqlite" "${query}")
echo "${todays_urls}" | tr ' ' '\n' | uniq > $mydir/todays_urls_new.txt
cat $HOME/firefox_urls.txt $mydir/todays_urls_new.txt | sort | uniq > $HOME/firefox_urls.txt
rm -rf $mydir
