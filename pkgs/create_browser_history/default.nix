{stdenv, lib}:
{
  create_browser_history = stdenv.mkDerivation rec {
    pname = "create_browser_history";
    version = "0.0.1";
    src = ./src;

    dontConfigure = true;
    dontBuild = true;
    dontPatch = true;

    installPhase = ''
      mkdir -p $out/bin
      cp create_browser_history.sh $out/bin/create_browser_history
    '';

    meta = with lib; {
      description = "Capture daily firefox history to a file";
      license = licenses.mit;
      platforms = platforms.all;
      maintainers = [ "jgillis" ];
    };
  };
}

