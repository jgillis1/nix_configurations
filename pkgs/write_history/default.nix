{buildGoModule, lib}:
{
  history_log =  buildGoModule rec {
    pname = "history_log";
    version = "0.0.1";

    src = builtins.fetchGit {
      url = "https://gitlab.com/jgillis1/${pname}.git";
      ref = "refs/tags/v${version}";

    };
    vendorSha256 = "sha256:13g403i90c8kj0adb8bx79crbsmq6raljflln8nxnmss6pfcvnz8"; 

    subPackages = [ "." ]; 


    runVend = false; 

    doCheck = false;
    checkPhase = ''
    '';

    postInstall = ''
      cp write_history $out/bin/write_history
    '';

    meta = with lib; {
      description = "Create history log for later reference";
      maintainers = with maintainers; [ jgillis ];
      platforms = platforms.linux;
    };
  };
}
