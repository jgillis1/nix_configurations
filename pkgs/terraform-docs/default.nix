{ stdenv, lib, buildGoModule }:

{
  terraform-docs = buildGoModule rec {
    pname = "terraform-docs";
    version = "0.10.1";

    src = builtins.fetchGit {
      url = "https://github.com/terraform-docs/terraform-docs";
      ref = "refs/tags/v${version}";
    };

    vendorSha256 = "sha256:1ir9jff5pdas4s5jx3zjxj85p3s7hq7lpmcpz3jaq28pbn6fmqcs"; 

    subPackages = [ "." ]; 
    runVend = false; 

    doCheck = true;
    # checkPhase = ''
    # '';

    meta = with lib; {
      description = "A utility to generate documentation from Terraform modules in various output formats.";
      maintainers = with maintainers; [ "metmajer" "khos2ow" ];
      platforms = platforms.linux;
    };
  };
}
