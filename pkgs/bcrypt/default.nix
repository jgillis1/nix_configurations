{ stdenv, lib, buildGoModule }:

{
  bcrypt = buildGoModule rec {
    pname = "bcrypt";
    version = "0.0.1";

    src = builtins.fetchGit {
      url = "https://gitlab.com/jgillis1/bcrypt.git";
      ref = "refs/tags/v${version}";
    };

    vendorSha256 = "sha256:0626q7al63ryinxp7pl3a9h953dswyw19ppfw63n1k18f9qzz25w"; 

    subPackages = [ "." ]; 
    runVend = false; 

    doCheck = false;
    checkPhase = ''
    '';

    meta = with lib; {
      description = "Create bcrypt hash from the command line";
      maintainers = with maintainers; [ jgillis ];
      platforms = platforms.linux;
    };
  };
}
