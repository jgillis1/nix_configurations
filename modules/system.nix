{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    cascadia-code
    curl
    fira-code
    firefox
    htop
    plasma-browser-integration
    redshift
    redshift-plasma-applet
    linuxPackages.v4l2loopback
    (lua.withPackages(ps: with ps; [ busted luafilesystem ]))
    luarocks
    nerdfonts
    unzip
    vim
    wget
    xclip
    xsel
  ];

  fonts.fonts = with pkgs; [
    fira-code
    cascadia-code
    nerdfonts
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # List services that you want to enable:
  services.pcscd.enable = true;
  nixpkgs.config.allowUnfree = true;
  system.stateVersion = "21.03"; # Did you read the comment?
}
